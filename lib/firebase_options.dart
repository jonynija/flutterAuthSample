import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;

class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        return macos;
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyAV0yao-fmeHKMIya6VL_R0-fcJsBqzOKg',
    appId: '1:900071776237:web:802249a5b2b9c1fbc411e4',
    messagingSenderId: '900071776237',
    projectId: 'authsample-4fd39',
    authDomain: 'authsample-4fd39.firebaseapp.com',
    databaseURL: 'https://authsample-4fd39.firebaseio.com',
    storageBucket: 'authsample-4fd39.appspot.com',
    measurementId: 'G-GBN7CG1BR4',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyA8pciDv-5QHAZhpB6vWRN1NJs5xpjxSJQ',
    appId: '1:900071776237:android:7504728ad679280bc411e4',
    messagingSenderId: '900071776237',
    projectId: 'authsample-4fd39',
    databaseURL: 'https://authsample-4fd39.firebaseio.com',
    storageBucket: 'authsample-4fd39.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyAtdX2j0T3W2hDUCE2OWqCffXt3xlnGtGs',
    appId: '1:900071776237:ios:b65fae5ba3897163c411e4',
    messagingSenderId: '900071776237',
    projectId: 'authsample-4fd39',
    databaseURL: 'https://authsample-4fd39.firebaseio.com',
    storageBucket: 'authsample-4fd39.appspot.com',
    androidClientId:
        '900071776237-3s9457etu6g4q294lmo6dr9ia7cb0f50.apps.googleusercontent.com',
    iosClientId:
        '900071776237-u20qgukadnncp8sroi9r20gea5p8omar.apps.googleusercontent.com',
    iosBundleId: 'com.jixcayau.auth',
  );

  static const FirebaseOptions macos = FirebaseOptions(
    apiKey: 'AIzaSyAtdX2j0T3W2hDUCE2OWqCffXt3xlnGtGs',
    appId: '1:900071776237:ios:fa7fe58ddd5ce22ac411e4',
    messagingSenderId: '900071776237',
    projectId: 'authsample-4fd39',
    databaseURL: 'https://authsample-4fd39.firebaseio.com',
    storageBucket: 'authsample-4fd39.appspot.com',
    androidClientId:
        '900071776237-3s9457etu6g4q294lmo6dr9ia7cb0f50.apps.googleusercontent.com',
    iosClientId:
        '900071776237-f0nc4ljrbg8nmon2u3jd5h9pk4e7f57n.apps.googleusercontent.com',
    iosBundleId: 'com.example.flutterAuthSample',
  );
}
