import 'package:flutter/material.dart';

/// Class only have been to be created
/// only one time per page to avoid presistent dialog
/// when use setState(() {});
class ProgressDialog {
  late BuildContext _context;
  bool _state = false;

  ProgressDialog(BuildContext context) {
    _context = context;
  }

  IconButton? _iconClose;

  void show() async {
    if (!_state) {
      _state = true;
      showDialog(
        context: _context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          /// WillPopScope is used to avoid close dialog on back

          return WillPopScope(
            child: AlertDialog(
              backgroundColor: Colors.transparent,
              content: Builder(
                builder: (context) {
                  _iconClose = IconButton(
                    icon: const Icon(Icons.close),
                    onPressed: () => Navigator.pop(context),
                  );

                  return Container(
                    height: 75,
                    width: 75,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: Colors.white,
                    ),
                    child: Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(
                          Theme.of(_context).primaryColor,
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
            onWillPop: () => false as Future<bool>,
          );
        },
      ).then((value) {
        _state = false;
      });
    }
  }

  void hide() async {
    try {
      if (_state) {
        if (_iconClose == null) {
          await Future.delayed(const Duration(milliseconds: 100));
        }

        _iconClose!.onPressed!();

        _iconClose = null;
      }
    } catch (e) {
      print(e);
    }
  }

  bool isShowing() {
    return _state;
  }
}
