import 'package:flutter/material.dart';

import 'package:auth/localization/internationalization.dart';
import 'package:auth/utils/Utils.dart';

commonOkDialog(
  BuildContext context,
  String? msg, {
  bool cancel = true,
  Function? okFunction,
}) {
  showDialog(
    context: context,
    barrierDismissible: cancel,
    builder: (BuildContext ct) {
      return AlertDialog(
        content: Text(
          msg!,
          textAlign: TextAlign.center,
          // style: Theme.of(context).textTheme.bodyText2,
        ),
        actions: [
          (!cancel && okFunction != null)
              ? TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text(
                    Internationalization(context)
                        .getString(LocaleKey.cancelKey)!,
                    // style: Theme.of(context).textTheme.bodyText2,
                  ),
                )
              : const SizedBox(),
          TextButton(
            onPressed: () {
              Navigator.pop(context);
              if (okFunction != null) okFunction();
            },
            child: Text(
              Internationalization(context).getString(LocaleKey.acceptKey)!,
              // style: Theme.of(context).textTheme.bodyText2,
            ),
          )
        ],
      );
    },
  );
}

Future<bool?> showExitApp(BuildContext context) async {
  final intl = Internationalization(context);

  return await showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: Text(
        intl.getString(LocaleKey.areYouSureKey)!,
        // style: textLigthStyle,
      ),
      content: Text(
        intl.getString(LocaleKey.exitAppKey)!,
        // style: textLigthStyle,
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.of(context).pop(false),
          child: Text(
            intl.getString(LocaleKey.cancelKey)!,
            // style: textLigthStyle,
          ),
        ),
        TextButton(
          onPressed: () => Navigator.of(context).pop(true),
          child: Text(
            intl.getString(LocaleKey.continueKey)!,
            // style: textLigthStyle,
          ),
        ),
      ],
    ),
  );
}
