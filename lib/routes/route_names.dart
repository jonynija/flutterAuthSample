import 'package:flutter/material.dart' as material;

import 'package:sailor/sailor.dart';

import 'package:auth/model/route/route.dart';
import 'package:auth/routes/routes.dart';
import 'package:auth/screens/auth/login/login_email_password_page.dart';
import 'package:auth/screens/auth/login/login_options_page.dart';
import 'package:auth/screens/auth/register/register_page.dart';
import 'package:auth/screens/config/config_page.dart';
import 'package:auth/screens/dashboard/dashboard_page.dart';
import 'package:auth/screens/dashboard/update_info_page.dart';
import 'package:auth/screens/dashboard/user_map_location_page.dart';
import 'package:auth/screens/utils/loading/loading_page.dart';

abstract class RouteNames {
  static const String initialLoadingRoute = '/loading';
  static const String loginOptionsRoute = '/loginOptions';
  static const String loginWithEmailAndPasswordRoute =
      '/loginWithEmailAndPassword';
  static const String registerRoute = '/register';
  static const String dashboardRoute = '/dashboard';
  static const String configRoute = '/config';
  static const String updateInfoRoute = '/updateInfo';
  static const String mapHomeLocationRoute = '/mapHomeLocation';
}

abstract class RouteUtils {
  static const List<Route> listRoutes = [
    Route(
      RouteNames.initialLoadingRoute,
      LoadingPage(),
    ),
    Route(
      RouteNames.loginOptionsRoute,
      LoginOptionsPage(),
    ),
    Route(
      RouteNames.loginWithEmailAndPasswordRoute,
      LoginWithEmailAndPassword(),
    ),
    Route(
      RouteNames.registerRoute,
      RegisterPage(),
    ),
    Route(
      RouteNames.dashboardRoute,
      DashboardPage(),
    ),
    Route(
      RouteNames.configRoute,
      ConfigPage(),
    ),
    Route(
      RouteNames.updateInfoRoute,
      UpdateInfoPage(),
    ),
    Route(
      RouteNames.mapHomeLocationRoute,
      UserMapLocationPage(),
    ),
  ];

  static Future navigateToPage(String route, {back = true}) {
    if (back) {
      return Routes.sailor.navigate(route);
    } else {
      return Routes.sailor.navigate(
        route,
        navigationType: NavigationType.pushAndRemoveUntil,
        removeUntilPredicate: (material.Route<dynamic> route) => false,
      );
    }
  }

  static void popPage() {
    Routes.sailor.pop();
  }
}
