import 'package:flutter/material.dart';

import 'package:auth/utils/Styles.dart';
import 'package:auth/utils/Utils.dart';

abstract class CustomTheme {
  static ThemeData get getLigth => ThemeData(
        scaffoldBackgroundColor: LigthColors.primaryColor,
        brightness: Brightness.light,
        primaryColor: LigthColors.primaryColor,
        primaryColorLight: LigthColors.primaryLightColor,
        primaryColorDark: LigthColors.primaryDarkColor,
        textSelectionTheme: const TextSelectionThemeData(
          selectionHandleColor: Colors.white,
        ),
        //
        visualDensity: VisualDensity.standard,
        //
        appBarTheme: AppBarTheme(
          color: LigthColors.primaryColor,
          iconTheme: const IconThemeData(
            color: Colors.white,
          ),
        ),
        //
        textTheme: TextTheme(
          // Principal title white
          displayLarge: headlineStyle.copyWith(color: whiteColor),
          // Principal title primary
          displayMedium:
              headlineStyle.copyWith(color: LigthColors.primaryColor),
          // Commont text primary
          bodyLarge: bodyStyle.copyWith(color: LigthColors.primaryColor),
          // Commont text white
          bodyMedium: bodyStyle.copyWith(color: whiteColor),
          //
          labelLarge: const TextStyle(
            color: Colors.white,
          ),
        ),
        //
        iconTheme: IconThemeData(
          color: LigthColors.primaryColor,
        ),
        //
        unselectedWidgetColor: Colors.black54,
        //
        inputDecorationTheme: InputDecorationTheme(
          fillColor: LigthColors.primaryColor,
          focusColor: LigthColors.primaryColor,
          hoverColor: LigthColors.primaryColor,
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: LigthColors.primaryColor),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: LigthColors.primaryColor),
          ),
          border: UnderlineInputBorder(
            borderSide: BorderSide(color: LigthColors.primaryColor),
          ),
          disabledBorder: UnderlineInputBorder(
            borderSide:
                BorderSide(color: LigthColors.primaryColor.withOpacity(0.5)),
          ),
        ),

        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: LigthColors.primarySwatch,
          brightness: Brightness.light,
        ).copyWith(
          secondary: LigthColors.accentColor,
        ),
      );

  static ThemeData get getDark => ThemeData(
        scaffoldBackgroundColor: DarkColors.primaryColor,
        brightness: Brightness.dark,
        primaryColor: DarkColors.primaryColor,
        primaryColorLight: DarkColors.primaryLightColor,
        primaryColorDark: DarkColors.primaryDarkColor,
        textSelectionTheme: const TextSelectionThemeData(
          selectionHandleColor: Colors.white,
        ),
        //
        visualDensity: VisualDensity.standard,
        //
        appBarTheme: AppBarTheme(
          color: DarkColors.primaryColor,
          iconTheme: const IconThemeData(
            color: Colors.white,
          ),
        ),
        //
        textTheme: TextTheme(
          // Principal title white
          displayLarge: headlineStyle.copyWith(color: whiteColor),
          // Principal title primary
          displayMedium: headlineStyle.copyWith(color: DarkColors.primaryColor),
          // Commont text primary
          bodyLarge: bodyStyle.copyWith(color: DarkColors.primaryColor),
          // Commont text white
          bodyMedium: bodyStyle.copyWith(color: whiteColor),
          //
          labelLarge: const TextStyle(
            color: Colors.white,
          ),
        ),
        //
        iconTheme: IconThemeData(
          color: DarkColors.primaryColor,
        ),
        //
        unselectedWidgetColor: Colors.black54,
        //
        inputDecorationTheme: InputDecorationTheme(
          fillColor: DarkColors.primaryColor,
          focusColor: DarkColors.primaryColor,
          hoverColor: DarkColors.primaryColor,
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: DarkColors.primaryColor),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: DarkColors.primaryColor),
          ),
          border: UnderlineInputBorder(
            borderSide: BorderSide(color: DarkColors.primaryColor),
          ),
          disabledBorder: UnderlineInputBorder(
            borderSide:
                BorderSide(color: DarkColors.primaryColor.withOpacity(0.5)),
          ),
        ),
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: DarkColors.primarySwatch,
          brightness: Brightness.dark,
        ).copyWith(
          secondary: DarkColors.accentColor,
        ),
      );
}
