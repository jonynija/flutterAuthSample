import 'dart:io';

import 'package:flutter/foundation.dart' show kIsWeb;

import 'package:firebase_storage/firebase_storage.dart';

class StorageService {
  Future<String?> updateImageFile(String path, File? file) async {
    try {
      Reference reference = FirebaseStorage.instance.ref().child(path);

      final metadata = SettableMetadata(contentType: 'image/jpeg');

      if (kIsWeb) {
        await reference.putData(await file!.readAsBytes(), metadata);
      } else {
        await reference.putFile(File(file!.path), metadata);
      }

      return await reference.getDownloadURL();
    } on FirebaseException catch (e) {
      print(e);
    }

    return null;
  }
}
