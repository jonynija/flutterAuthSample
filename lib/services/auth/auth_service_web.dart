import 'package:auth/services/auth/base/base_auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class AuthServiceWeb extends BaseAuth {
  @override
  Future<UserCredential> loginFacebook() async {
    FacebookAuthProvider facebookProvider = FacebookAuthProvider();

    facebookProvider.addScope('email');
    facebookProvider.setCustomParameters({'display': 'popup'});

    return await FirebaseAuth.instance.signInWithPopup(facebookProvider);
  }

  @override
  Future<UserCredential> loginGoogle() async {
    GoogleAuthProvider googleProvider = GoogleAuthProvider();

    return await FirebaseAuth.instance.signInWithPopup(googleProvider);
  }

  @override
  Future<UserCredential> loginTwitter() async {
    TwitterAuthProvider twitterProvider = TwitterAuthProvider();

    return await FirebaseAuth.instance.signInWithPopup(twitterProvider);
  }

  @override
  Future<UserCredential> loginGithub(BuildContext context) async {
    GithubAuthProvider githubProvider = GithubAuthProvider();

    return await FirebaseAuth.instance.signInWithPopup(githubProvider);
  }
}
