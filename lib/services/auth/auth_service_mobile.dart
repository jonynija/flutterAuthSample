import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'package:auth/services/auth/base/base_auth.dart';

class AuthServiceMobile extends BaseAuth {
  static String facebookAccessToken = '';

  @override
  Future<UserCredential?> loginFacebook() async {
    try {
      final loginResult = await FacebookAuth.instance.login();

      if (loginResult.accessToken?.token == null) {
        return null;
      }

      final facebookAuthCredential =
          FacebookAuthProvider.credential(loginResult.accessToken!.token);

      return FirebaseAuth.instance.signInWithCredential(facebookAuthCredential);
    } catch (e) {
      print(e);
    }

    return null;
  }

  @override
  Future<UserCredential?> loginGoogle() async {
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    if (googleUser == null) return null;

    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    final GoogleAuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    ) as GoogleAuthCredential;

    return await firebaseAuth!.signInWithCredential(credential);
  }

  // TODO: Missing correct twitter auth impl
  @override
  Future<UserCredential?> loginTwitter() async {
    /*  final twitterLogin = TwitterLogin(
      apiKey: 'JKomrf1RtbGcHKxb4w6arj6mQ',
      apiSecretKey: 'uooktNIjScTPDhOifO51yV2fqMFdda88l92aE3wYeycc3FHnu5',
      redirectURI: 'example://',
    );
    final authResult = await twitterLogin.login();

    switch (authResult.status) {
      case TwitterLoginStatus.loggedIn:
        final twitterAuthCredential = TwitterAuthProvider.credential(
          accessToken: authResult.authToken!,
          secret: authResult.authTokenSecret!,
        );

        return await firebaseAuth!.signInWithCredential(twitterAuthCredential);
      case TwitterLoginStatus.cancelledByUser:
      case TwitterLoginStatus.error:
      case null:
        return null;
    } */

    TwitterAuthProvider twitterProvider = TwitterAuthProvider();

    if (kIsWeb) {
      await FirebaseAuth.instance.signInWithPopup(twitterProvider);
    } else {
      await FirebaseAuth.instance.signInWithProvider(twitterProvider);
    }
    return null;

    /*   final TwitterLogin twitterLogin = TwitterLogin(
      consumerKey: "JKomrf1RtbGcHKxb4w6arj6mQ",
      consumerSecret: "uooktNIjScTPDhOifO51yV2fqMFdda88l92aE3wYeycc3FHnu5",
    );

    final TwitterLoginResult result = await twitterLogin.authorize();

    if (result == null ||
        result.status == TwitterLoginStatus.cancelledByUser ||
        result.status == TwitterLoginStatus.error) return null; */
  }

  @override
  Future<UserCredential?> loginGithub(BuildContext context) async {
    // TODO: Missing migrate package
    /* final GitHubSignIn gitHubSignIn = GitHubSignIn(
      clientId: "ef02311ad2c50fc4117b",
      clientSecret: "989152ba1ebb5433e9cf971a1c58411ef2b2e26b",
      redirectUrl: "https://authsample-4fd39.firebaseapp.com/__/auth/handler",
    );

    final GitHubSignInResult result = await gitHubSignIn.signIn(context);

    if (result == null ||
        result.status == GitHubSignInResultStatus.cancelled ||
        result.status == GitHubSignInResultStatus.failed) return null;

    final AuthCredential githubCredential =
        GithubAuthProvider.credential(result.token);

    return await firebaseAuth.signInWithCredential(githubCredential); */
    return null;
  }
}
