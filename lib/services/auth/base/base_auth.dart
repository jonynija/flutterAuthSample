import 'package:flutter/material.dart';

import 'package:firebase_auth/firebase_auth.dart';

abstract class BaseAuth {
  BaseAuth() {
    firebaseAuth = FirebaseAuth.instance;
  }

  FirebaseAuth? firebaseAuth;

  Future<UserCredential> signInAnonymously() async {
    return await FirebaseAuth.instance.signInAnonymously();
  }

  Future<UserCredential?> signInWithEmailAndPassword(
    String email,
    String pass,
  ) async {
    try {
      return await firebaseAuth!
          .signInWithEmailAndPassword(email: email, password: pass);
    } catch (e) {
      print(e);
    }

    return null;
  }

  Future<UserCredential?> createUserWithEmailAndPassword(
    String email,
    String pass,
  ) async {
    try {
      return await firebaseAuth!.createUserWithEmailAndPassword(
        email: email,
        password: pass,
      );
    } catch (e) {
      print(e);
    }

    return null;
  }

  // ignore: missing_return
  Future<UserCredential?> loginFacebook();
  // ignore: missing_return
  Future<UserCredential?> loginGoogle();
  // ignore: missing_return
  Future<UserCredential?> loginTwitter();
  // ignore: missing_return
  Future<UserCredential?> loginGithub(BuildContext context);

  Future<void> signOut() async {
    return await firebaseAuth!.signOut();
  }
}
