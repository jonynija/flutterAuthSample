import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';

import 'package:firebase_auth/firebase_auth.dart';

import 'package:auth/services/auth/auth_service_mobile.dart';
import 'package:auth/services/auth/auth_service_web.dart';
import 'package:auth/services/auth/base/base_auth.dart';

class AuthService implements BaseAuth {
  late BaseAuth _auth;

  AuthService() {
    _auth = (kIsWeb) ? AuthServiceWeb() : AuthServiceMobile();
  }

  @override
  FirebaseAuth? firebaseAuth;

  @override
  Future<UserCredential?> createUserWithEmailAndPassword(
    String email,
    String pass,
  ) async {
    try {
      return await _auth.createUserWithEmailAndPassword(email, pass);
    } catch (e) {
      print(e);
    }

    return null;
  }

  @override
  Future<UserCredential?> loginFacebook() {
    return _auth.loginFacebook();
  }

  @override
  Future<UserCredential?> loginGithub(BuildContext context) {
    return _auth.loginGithub(context);
  }

  @override
  Future<UserCredential?> loginGoogle() {
    return _auth.loginGoogle();
  }

  @override
  Future<UserCredential?> loginTwitter() {
    return _auth.loginTwitter();
  }

  @override
  Future<UserCredential?> signInWithEmailAndPassword(
    String email,
    String pass,
  ) {
    return _auth.signInWithEmailAndPassword(email, pass);
  }

  @override
  Future<void> signOut() {
    return _auth.signOut();
  }

  @override
  Future<UserCredential> signInAnonymously() {
    return _auth.signInAnonymously();
  }
}
