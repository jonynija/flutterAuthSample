import 'package:flutter/material.dart';

import 'package:firebase_auth/firebase_auth.dart';

import 'package:auth/routes/route_names.dart';

class LoadingPage extends StatefulWidget {
  const LoadingPage({
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> {
  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, _initialPage);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: const Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  void _initialPage() {
    final User? user = FirebaseAuth.instance.currentUser;

    final String routePath = (user == null)
        ? RouteNames.loginOptionsRoute
        : RouteNames.dashboardRoute;

    RouteUtils.navigateToPage(routePath, back: false);
  }
}
