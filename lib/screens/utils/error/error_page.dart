import 'package:flutter/material.dart';

import 'package:firebase_crashlytics/firebase_crashlytics.dart';

import 'package:auth/localization/internationalization.dart';
import 'package:auth/utils/locale_codes.dart';
import 'package:auth/widgets/base_scroll.dart';
import 'package:auth/widgets/common_icon.dart';
import 'package:auth/widgets/text/common_text.dart';

class ErrorPage extends StatefulWidget {
  final FlutterErrorDetails error;

  const ErrorPage(
    this.error, {
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _ErrorPageState();
}

class _ErrorPageState extends State<ErrorPage> {
  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, _initPage);
  }

  @override
  Widget build(BuildContext context) {
    return BaseScroll(
      backgroundColor: Theme.of(context).primaryColor,
      children: [
        const CommonIcon(
          Icons.warning,
          customColor: Colors.red,
          size: 64,
        ),
        const SizedBox(height: 15),
        CommonText(
          Internationalization(context).getString(LocaleKey.appFailedKey),
          style: Theme.of(context).textTheme.displayLarge,
        ),
      ],
    );
  }

  void _initPage() {
    FirebaseCrashlytics.instance.log(widget.error.exception.toString());
  }
}
