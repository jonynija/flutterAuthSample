import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';

import 'package:auth/common/dialogs/common_dialogs.dart';
import 'package:auth/common/progress_dialog/progress_dialog.dart';
import 'package:auth/enums/auth_type.dart';
import 'package:auth/localization/internationalization.dart';
import 'package:auth/model/user/user.dart' as usr;
import 'package:auth/provider/user/user_provider.dart';
import 'package:auth/routes/route_names.dart';
import 'package:auth/services/auth/auth_service.dart';
import 'package:auth/services/auth/auth_service_mobile.dart';
import 'package:auth/utils/Utils.dart';
import 'package:auth/utils/error/auth_errors.dart';
import 'package:auth/widgets/base_scroll.dart';
import 'package:auth/widgets/common_button.dart';
import 'package:auth/widgets/on_close_app.dart';
import 'package:auth/widgets/social_button.dart';

class LoginOptionsPage extends StatefulWidget {
  const LoginOptionsPage({
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _LoginOptionsPageState();
}

class _LoginOptionsPageState extends State<LoginOptionsPage> {
  late ProgressDialog _pr;
  late Internationalization _int;

  late UserProvider _userProvider;

  final AuthService _auth = AuthService();

  AuthType? _type;

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, _initPage);
  }

  @override
  Widget build(BuildContext context) {
    _int = Internationalization(context);

    _userProvider = Provider.of<UserProvider>(context);

    return OnCloseApp(
      child: Scaffold(
        body: Stack(
          alignment: Alignment.topRight,
          children: [
            BaseScroll(
              safeArea: false,
              backgroundColor: Theme.of(context).primaryColor,
              children: [
                CommonButton(
                  text: _int.getString(LocaleKey.emailPasswordSigninKey),
                  callback: () => RouteUtils.navigateToPage(
                    RouteNames.loginWithEmailAndPasswordRoute,
                  ),
                ),
                CommonButton(
                  callback: _navigateToRegister,
                  text: _int.getString(LocaleKey.registerKey),
                ),
                CommonButton(
                  callback: _signinAnonymus,
                  text: _int.getString(LocaleKey.signinAnonymusKey),
                ),
                const SizedBox(height: 30),
                const SocialButton(
                  callback: null,
                  type: AuthType.apple,
                ),
                SocialButton(
                  callback: () => _loginWith(AuthType.google),
                  type: AuthType.google,
                ),
                SocialButton(
                  callback: () => _loginWith(AuthType.twitter),
                  type: AuthType.twitter,
                ),
                SocialButton(
                  callback: () => _loginWith(AuthType.facebook),
                  type: AuthType.facebook,
                ),
                SocialButton(
                  callback: () => _loginWith(AuthType.github),
                  type: AuthType.github,
                ),
              ],
            ),
            SafeArea(
              top: true,
              right: true,
              child: IconButton(
                icon: const Icon(
                  Icons.settings,
                  color: Colors.white,
                ),
                onPressed: () =>
                    RouteUtils.navigateToPage(RouteNames.configRoute),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _initPage() {
    _pr = ProgressDialog(context);
  }

  void _navigateToRegister() {
    RouteUtils.navigateToPage(RouteNames.registerRoute);
  }

  void _signinAnonymus() {
    commonOkDialog(
      context,
      _int.getString(LocaleKey.signinAnonymusAlertKey),
      cancel: false,
      okFunction: () => _loginWith(AuthType.anonimus),
    );
  }

  void _loginWith(AuthType type) async {
    _type = type;

    _pr.show();
    try {
      UserCredential? userCredential;

      switch (type) {
        case AuthType.anonimus:
          userCredential = await _auth.signInAnonymously();
          break;
        case AuthType.facebook:
          userCredential = await _auth.loginFacebook();
          break;
        case AuthType.google:
          userCredential = await _auth.loginGoogle();
          break;
        case AuthType.twitter:
          userCredential = await _auth.loginTwitter();
          break;
        case AuthType.github:
          userCredential = await _auth.loginGithub(context);
          break;
        default:
          break;
      }

      if (userCredential != null) {
        _getUserInfo(userCredential);
      } else {
        _pr.hide();
      }
    } on FirebaseAuthException catch (e) {
      _pr.hide();

      print('UserInfo: AUTH ERROR ----- AUTH ERROR');
      print(e);

      AuthErrors().showUserError(context, e.code);
    } catch (e) {
      _pr.hide();
      print(e);
    }
  }

  void _getUserInfo(UserCredential credential) async {
    await _userProvider.requestUser(credential.user!.uid).then((user) async {
      if (user != null) {
        _userProvider.setUser = user;

        _navigateToDashboard();
      } else {
        _registerUser(credential);
      }
    }).catchError((error) {
      print(error);
    });
  }

  void _registerUser(UserCredential credential) async {
    String? photoUrl = credential.user!.photoURL;

    if (_type == AuthType.facebook) {
      String? token = '';

      if (kIsWeb) {
        Map<String, dynamic> authCredentialMap = credential.credential!.asMap();

        for (var entry in authCredentialMap.entries) {
          if (entry.key == 'accessToken') {
            token = entry.value;
            break;
          }
        }
      } else {
        token = AuthServiceMobile.facebookAccessToken;
      }
      if (photoUrl != null) {
        photoUrl += '?height=500&access_token=$token';
      }
    }

    usr.User user = usr.User(
      id: credential.user!.uid,
      name: credential.user!.displayName,
      email: credential.user!.email,
      profilePicture: photoUrl,
    );

    await _userProvider.createUser(user).then((value) async {
      final newUser = await _userProvider.requestUser(credential.user!.uid);

      if (newUser != null) {
        user = newUser;
      }

      _userProvider.setUser = user;

      _navigateToDashboard();
    }).catchError((error) {
      _pr.hide();
      commonOkDialog(context, error);
    });
  }

  void _navigateToDashboard() {
    _pr.hide();
    RouteUtils.navigateToPage(RouteNames.dashboardRoute, back: false);
  }
}
