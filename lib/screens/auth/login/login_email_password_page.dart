import 'package:flutter/material.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';

import 'package:auth/common/progress_dialog/progress_dialog.dart';
import 'package:auth/localization/internationalization.dart';
import 'package:auth/provider/user/user_provider.dart';
import 'package:auth/routes/route_names.dart';
import 'package:auth/services/auth/auth_service.dart';
import 'package:auth/utils/Utils.dart';
import 'package:auth/utils/error/auth_errors.dart';
import 'package:auth/widgets/base_scroll.dart';
import 'package:auth/widgets/common_button.dart';
import 'package:auth/widgets/common_icon.dart';
import 'package:auth/widgets/common_input.dart';

class LoginWithEmailAndPassword extends StatefulWidget {
  const LoginWithEmailAndPassword({
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _LoginWithEmailAndPasswordState();
}

class _LoginWithEmailAndPasswordState extends State<LoginWithEmailAndPassword> {
  late ProgressDialog _pr;
  late Internationalization _int;

  late UserProvider _userProvider;

  final AuthService _auth = AuthService();

  TextEditingController? _userController;
  TextEditingController? _passwordController;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();

    _userController = TextEditingController(text: '');
    _passwordController = TextEditingController(text: '');

    Future.delayed(Duration.zero, _initPage);
  }

  @override
  Widget build(BuildContext context) {
    _int = Internationalization(context);

    _userProvider = Provider.of<UserProvider>(context);

    return Scaffold(
      body: BaseScroll(
        safeArea: false,
        backgroundColor: Theme.of(context).primaryColor,
        children: [
          Image.asset(
            'assets/logo.png',
            width: 100,
            height: 100,
          ),
          const SizedBox(height: 40),
          Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                CommonInput(
                  isEmail: true,
                  controller: _userController,
                  prefixIcon: const CommonIcon(Icons.person, isWhite: true),
                  hint: _int.getString(LocaleKey.emailKey),
                  isDark: true,
                  type: TextInputType.emailAddress,
                ),
                const SizedBox(height: 10),
                CommonInput(
                  controller: _passwordController,
                  hint: _int.getString(LocaleKey.passwordKey),
                  obscureText: true,
                  prefixIcon: const CommonIcon(Icons.lock, isWhite: true),
                  isDark: true,
                  type: TextInputType.visiblePassword,
                ),
              ],
            ),
          ),
          const SizedBox(height: 20),
          CommonButton(
            text: _int.getString(LocaleKey.continueKey),
            callback: _validateForm,
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }

  void _initPage() {
    _pr = ProgressDialog(context);
  }

  void _validateForm() async {
    if (_formKey.currentState!.validate()) {
      _loginWithEmail();
    }
  }

  void _loginWithEmail() async {
    _pr.show();

    try {
      final String email = _userController!.text.trim();
      final String password = _passwordController!.text.trim();

      final userCredential =
          await _auth.signInWithEmailAndPassword(email, password);

      if (userCredential != null) {
        _getUserInfo(userCredential);
      } else {
        _pr.hide();
      }
    } on FirebaseAuthException catch (e) {
      _pr.hide();

      print('UserInfo: AUTH ERROR ----- AUTH ERROR');
      print(e);

      AuthErrors().showUserError(context, e.code);
    }
  }

  void _getUserInfo(UserCredential credential) async {
    await _userProvider.requestUser(credential.user!.uid).then((user) async {
      if (user != null) {
        _userProvider.setUser = user;

        _navigateToDashboard();
      }
    });
  }

  void _navigateToDashboard() {
    _pr.hide();
    RouteUtils.navigateToPage(RouteNames.dashboardRoute, back: false);
  }
}
