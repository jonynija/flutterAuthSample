import 'dart:async';

import 'package:flutter/material.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';

import 'package:auth/common/dialogs/common_dialogs.dart';
import 'package:auth/common/progress_dialog/progress_dialog.dart';
import 'package:auth/localization/internationalization.dart';
import 'package:auth/model/user/user.dart' as usr;
import 'package:auth/provider/user/user_provider.dart';
import 'package:auth/routes/route_names.dart';
import 'package:auth/services/auth/auth_service.dart';
import 'package:auth/utils/utils.dart';
import 'package:auth/widgets/base_scroll.dart';
import 'package:auth/widgets/common_button.dart';
import 'package:auth/widgets/common_icon.dart';
import 'package:auth/widgets/common_input.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  late Internationalization _int;
  late ProgressDialog _pr;

  // Provider
  late UserProvider _userProvider;

  final AuthService _auth = AuthService();

  TextEditingController? _userController;
  TextEditingController? _passwordController;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void initState() {
    super.initState();

    _userController = TextEditingController(text: '');
    _passwordController = TextEditingController(text: '');

    Future.delayed(Duration.zero, _initPage);
  }

  @override
  Widget build(BuildContext context) {
    _int = Internationalization(context);

    _userProvider = Provider.of<UserProvider>(context);

    return Scaffold(
      body: BaseScroll(
        safeArea: false,
        backgroundColor: Theme.of(context).primaryColor,
        children: [
          Image.asset(
            'assets/logo.png',
            width: 100,
            height: 100,
          ),
          const SizedBox(height: 40),
          Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                CommonInput(
                  isEmail: true,
                  prefixIcon: const CommonIcon(Icons.person, isWhite: true),
                  controller: _userController,
                  hint: _int.getString(LocaleKey.emailKey),
                  isDark: true,
                  type: TextInputType.emailAddress,
                ),
                const SizedBox(height: 10),
                CommonInput(
                  controller: _passwordController,
                  hint: _int.getString(LocaleKey.passwordKey),
                  obscureText: true,
                  prefixIcon: const CommonIcon(Icons.lock, isWhite: true),
                  isDark: true,
                  type: TextInputType.visiblePassword,
                ),
              ],
            ),
          ),
          const SizedBox(height: 20),
          CommonButton(
            text: _int.getString(LocaleKey.continueKey),
            callback: _validateForm,
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }

  void _initPage() {
    _pr = ProgressDialog(context);
  }

  void _validateForm() async {
    if (_formKey.currentState!.validate()) {
      try {
        _pr.show();

        final String email = _userController!.text.trim();
        final String password = _passwordController!.text.trim();

        final userCredential =
            await _auth.createUserWithEmailAndPassword(email, password);

        if (userCredential != null) {
          _registerUser(userCredential);
        } else {
          _pr.hide();
        }
      } on FirebaseAuthException catch (e) {
        _pr.hide();
        _showUserError(e.code);
      }
    }
  }

  void _registerUser(UserCredential credential) async {
    usr.User user = usr.User(
      id: credential.user!.uid,
      name: credential.user!.displayName,
      email: credential.user!.email,
      profilePicture: credential.user!.photoURL,
    );

    await _userProvider.createUser(user).then((value) async {
      user = await (_userProvider.requestUser(credential.user!.uid)
          as FutureOr<usr.User>);
      _userProvider.setUser = user;

      _navigateToDashboard();
    }).catchError((error) {
      _pr.hide();
      commonOkDialog(context, error.toString());
    });
  }

  void _navigateToDashboard() {
    _pr.hide();
    RouteUtils.navigateToPage(RouteNames.dashboardRoute, back: false);
  }

  void _showUserError(String err) {
    String? error = '';

    switch (err) {
      case 'user-not-found':
        error = _int.getString(LocaleKey.userNotFoundKey);
        break;
      case 'account-exists-with-different-credential':
        error = _int.getString(LocaleKey.registeredDifferentMethodKey);
        break;
      case 'wrong-password':
        error = _int.getString(LocaleKey.wrongPasswordKey);
        break;
      default:
        print(err);
    }
    commonOkDialog(context, error);
  }
}
