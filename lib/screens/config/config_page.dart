import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:auth/localization/internationalization.dart';
import 'package:auth/provider/theme/app_theme_provider.dart';
import 'package:auth/utils/locale_codes.dart';
import 'package:auth/utils/theme_codes.dart';
import 'package:auth/widgets/base_scroll.dart';
import 'package:auth/widgets/common_appbar.dart';

class ConfigPage extends StatefulWidget {
  const ConfigPage({
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _ConfigPageState();
}

class _ConfigPageState extends State<ConfigPage> {
  late Internationalization _int;
  late AppThemeProvider _themeProvider;

  late SharedPreferences _prefs;

  ThemeMode? _themeMode = ThemeMode.system;

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, _initPage);
  }

  @override
  Widget build(BuildContext context) {
    _int = Internationalization(context);
    _themeProvider = Provider.of<AppThemeProvider>(context);

    return Scaffold(
      appBar: const CommonAppbar(showLanguage: true),
      body: BaseScroll(
        children: [
          RadioListTile<ThemeMode>(
            title: Text(
              _int.getString(LocaleKey.darkThemeKey)!,
              style: Theme.of(context).textTheme.bodyLarge,
            ),
            value: ThemeMode.dark,
            groupValue: _themeMode,
            onChanged: _updateTheme,
          ),
          RadioListTile<ThemeMode>(
            title: Text(
              _int.getString(LocaleKey.ligthThemeKey)!,
              style: Theme.of(context).textTheme.bodyLarge,
            ),
            value: ThemeMode.light,
            groupValue: _themeMode,
            onChanged: _updateTheme,
          ),
          RadioListTile<ThemeMode>(
            title: Text(
              _int.getString(LocaleKey.systemThemeKey)!,
              style: Theme.of(context).textTheme.bodyLarge,
            ),
            value: ThemeMode.system,
            groupValue: _themeMode,
            onChanged: _updateTheme,
          ),
        ],
      ),
    );
  }

  void _initPage() async {
    _prefs = await SharedPreferences.getInstance();

    final String? mode = _prefs.getString(ThemeCodes.themeSaved);

    _themeMode = _themeProvider.getThemeMode(mode);
    setState(() {});
  }

  void _updateTheme(ThemeMode? value) {
    _themeMode = value;
    _prefs.setString(ThemeCodes.themeSaved, _themeProvider.getThemeStr(value));

    _themeProvider.setCurrentMode = _themeMode;

    setState(() {});
  }
}
