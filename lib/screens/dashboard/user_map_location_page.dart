import 'dart:async';
import 'dart:ui' as ui;

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

import 'package:auth/localization/internationalization.dart';
import 'package:auth/provider/user/user_provider.dart';
import 'package:auth/utils/locale_codes.dart';
import 'package:auth/widgets/common_appbar.dart';
import 'package:auth/widgets/common_button.dart';

class UserMapLocationPage extends StatefulWidget {
  const UserMapLocationPage({
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _UserMapLocationPageState();
}

class _UserMapLocationPageState extends State<UserMapLocationPage> {
  late Internationalization _int;

  late UserProvider _userProvider;

  final CameraPosition _initialPosition =
      const CameraPosition(target: LatLng(0, 0));
  final Completer<GoogleMapController> _controller = Completer();

  final Set<Marker> _markers = {};

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    _int = Internationalization(context);

    _userProvider = Provider.of<UserProvider>(context);

    return Scaffold(
      appBar: const CommonAppbar(),
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          GoogleMap(
            onMapCreated: _onMapCreated,
            initialCameraPosition: _initialPosition,
            myLocationButtonEnabled: false,
            myLocationEnabled: true,
            mapToolbarEnabled: false,
            zoomControlsEnabled: false,
            markers: _markers,
          ),
          SafeArea(
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 16),
              constraints: const BoxConstraints(maxWidth: 225),
              child: CommonButton(
                text: _int.getString(LocaleKey.homeKey),
                callback: _initialLocation,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);

    _initialLocation();
  }

  void _initialLocation() async {
    final GeoPoint geoPoint = _userProvider.getUser!.getHomeLocation!;

    final GoogleMapController controller = await _controller.future;
    final LatLng lang = LatLng(geoPoint.latitude, geoPoint.longitude);

    // Web doesn't allow custom icon for marker
    if (kIsWeb) {
      _markers.add(
        Marker(
          markerId: const MarkerId('Current'),
          position: lang,
        ),
      );
    } else {
      final Uint8List markerIcon =
          await _getBytesFromAsset('assets/icon/icon_location.png', 100);
      _markers.add(
        Marker(
          markerId: const MarkerId('Current'),
          position: lang,
          icon: BitmapDescriptor.fromBytes(markerIcon),
        ),
      );
    }

    controller.animateCamera(CameraUpdate.newLatLngZoom(lang, 14));
    setState(() {});
  }

  Future<Uint8List> _getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(
      data.buffer.asUint8List(),
      targetWidth: width,
    );
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
        .buffer
        .asUint8List();
  }
}
