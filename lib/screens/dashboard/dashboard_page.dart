import 'package:flutter/material.dart';

import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:provider/provider.dart';

import 'package:auth/common/dialogs/common_dialogs.dart';
import 'package:auth/common/progress_dialog/progress_dialog.dart';
import 'package:auth/localization/internationalization.dart';
import 'package:auth/model/user/user.dart';
import 'package:auth/provider/user/user_provider.dart';
import 'package:auth/routes/route_names.dart';
import 'package:auth/services/auth/auth_service.dart';
import 'package:auth/utils/Utils.dart';
import 'package:auth/widgets/base_scroll.dart';
import 'package:auth/widgets/common_appbar.dart';
import 'package:auth/widgets/common_button.dart';
import 'package:auth/widgets/load_image.dart';
import 'package:auth/widgets/on_close_app.dart';
import 'package:auth/widgets/text/common_text.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  late ProgressDialog _pr;
  late Internationalization _int;

  // Providers
  late UserProvider _userProvider;

  // Vars
  User? user = User();

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, _initPage);
  }

  @override
  Widget build(BuildContext context) {
    _int = Internationalization(context);

    _userProvider = Provider.of<UserProvider>(context);

    return OnCloseApp(
      child: Scaffold(
        appBar: CommonAppbar(
          trailing: IconButton(
            icon: const Icon(Icons.settings),
            onPressed: () => RouteUtils.navigateToPage(RouteNames.configRoute),
          ),
        ),
        body: BaseScroll(
          children: [
            (user != null && user!.getProfilePicture.isNotEmpty)
                ? LoadingImage(
                    user!.getProfilePicture,
                    width: 250,
                    height: 250,
                    circle: true,
                  )
                : Icon(
                    Icons.person,
                    color: Theme.of(context).primaryColor,
                    size: 100,
                  ),
            const SizedBox(height: 20),
            CommonText(user!.getDescription),
            const SizedBox(height: 10),
            CommonText(
              '${_int.getString(LocaleKey.nameKey)}: ${user!.getName}',
            ),
            CommonText(
              '${_int.getString(LocaleKey.emailKey)}: ${user!.getEmail}',
            ),
            CommonText(
              '${_int.getString(LocaleKey.addressKey)}: ${user!.getAddress}',
            ),
            (user!.getHomeLocation != null)
                ? CommonText(
                    '${_int.getString(LocaleKey.locationKey)}: ${user!.getHomeLocation!.latitude} / ${user!.getHomeLocation!.longitude}',
                  )
                : CommonText(_int.getString(LocaleKey.noLocationSavedKey)),
            (user!.getHomeLocation != null)
                ? CommonButton(
                    text: 'Show location on map',
                    callback: () => RouteUtils.navigateToPage(
                      RouteNames.mapHomeLocationRoute,
                    ),
                  )
                : const SizedBox(),
            const SizedBox(height: 40),
            CommonButton(
              text: _int.getString(LocaleKey.updateDataKey),
              callback: _navigateToUpdate,
            ),
            CommonButton(
              text: _int.getString(LocaleKey.logoutKey),
              callback: _showLogoutDialog,
            ),
          ],
        ),
      ),
    );
  }
  //

  void _initPage() {
    _pr = ProgressDialog(context);

    if (_userProvider.getUser != null) {
      user = _userProvider.getUser;
      setState(() {});
    } else {
      _loadUserInfo();
    }
  }

  void _loadUserInfo() async {
    _pr.show();
    auth.User firebaseUser = auth.FirebaseAuth.instance.currentUser!;

    user = await _userProvider.requestUser(firebaseUser.uid);

    _userProvider.setUser = user;

    _pr.hide();
    setState(() {});
  }

  ///

  void _navigateToUpdate() async {
    await RouteUtils.navigateToPage(RouteNames.updateInfoRoute);

    user = _userProvider.getUser;
    setState(() {});
  }

  ///

  void _showLogoutDialog() {
    commonOkDialog(
      context,
      _int.getString(LocaleKey.logoutKey),
      cancel: false,
      okFunction: _signOut,
    );
  }

  void _signOut() async {
    _pr.show();
    await AuthService().signOut().then((value) {
      _pr.hide();
      RouteUtils.navigateToPage(RouteNames.loginOptionsRoute, back: false);
    });
  }
}
