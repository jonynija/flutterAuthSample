import 'dart:io';

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';

import 'package:auth/common/dialogs/common_dialogs.dart';
import 'package:auth/common/progress_dialog/progress_dialog.dart';
import 'package:auth/localization/internationalization.dart';
import 'package:auth/model/user/user.dart';
import 'package:auth/provider/storage/storage_provider.dart';
import 'package:auth/provider/user/user_provider.dart';
import 'package:auth/routes/route_names.dart';
import 'package:auth/utils/Utils.dart';
import 'package:auth/utils/image_utils.dart';
import 'package:auth/widgets/base_scroll.dart';
import 'package:auth/widgets/circular_widget.dart';
import 'package:auth/widgets/common_appbar.dart';
import 'package:auth/widgets/common_button.dart';
import 'package:auth/widgets/common_icon.dart';
import 'package:auth/widgets/common_input.dart';
import 'package:auth/widgets/load_image.dart';

class UpdateInfoPage extends StatefulWidget {
  const UpdateInfoPage({
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _UpdateInfoPageState();
}

class _UpdateInfoPageState extends State<UpdateInfoPage> {
  late Internationalization _int;
  late ProgressDialog _pr;

  late UserProvider _userProvider;
  late StorageProvider _storageProvider;

  User? user;

  TextEditingController? _nameController;
  TextEditingController? _phoneController;
  TextEditingController? _addressController;
  TextEditingController? _descriptionController;
  TextEditingController? _geoController;

  File? _userImage;
  GeoPoint? geoPoint;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void initState() {
    super.initState();

    _nameController = TextEditingController(text: '');
    _phoneController = TextEditingController(text: '');
    _addressController = TextEditingController(text: '');
    _descriptionController = TextEditingController(text: '');
    _geoController = TextEditingController(text: '');

    Future.delayed(Duration.zero, _initPage);
  }

  @override
  Widget build(BuildContext context) {
    _int = Internationalization(context);

    _userProvider = Provider.of<UserProvider>(context);
    _storageProvider = Provider.of<StorageProvider>(context);

    return Scaffold(
      appBar: const CommonAppbar(),
      body: BaseScroll(
        children: [
          Text(
            _int.getString(LocaleKey.updateDataKey)!,
            style: Theme.of(context).textTheme.displayMedium,
          ),
          const SizedBox(height: 20),
          InkWell(
            onTap: _validateLocationImage,
            child: Stack(
              alignment: Alignment.bottomRight,
              children: [
                (_userImage != null)
                    ? CircularWidget(
                        child: Image.file(
                          _userImage!,
                          width: 250,
                          height: 250,
                          fit: BoxFit.fill,
                        ),
                      )
                    : (user != null && user!.getProfilePicture.isNotEmpty)
                        ? LoadingImage(
                            user!.getProfilePicture,
                            width: 250,
                            height: 250,
                            circle: true,
                          )
                        : SizedBox(
                            width: 150,
                            child: Icon(
                              Icons.person,
                              color: Theme.of(context).primaryColor,
                              size: 100,
                            ),
                          ),
                CircularWidget(
                  color: Theme.of(context).colorScheme.secondary,
                  child: IconButton(
                    icon: const Icon(Icons.add),
                    iconSize: 35,
                    color: Colors.white,
                    onPressed: _validateLocationImage,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 15),
          Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                CommonInput(
                  controller: _nameController,
                  hint: _int.getString(LocaleKey.nameKey),
                  prefixIcon: const CommonIcon(Icons.person),
                ),
                CommonInput(
                  controller: _phoneController,
                  hint: _int.getString(LocaleKey.phoneKey),
                  prefixIcon: const CommonIcon(Icons.phone),
                ),
                CommonInput(
                  controller: _addressController,
                  hint: _int.getString(LocaleKey.addressKey),
                  prefixIcon: const CommonIcon(Icons.directions),
                ),
                CommonInput(
                  controller: _descriptionController,
                  hint: _int.getString(LocaleKey.descriptionKey),
                  prefixIcon: const CommonIcon(Icons.description),
                ),
                CommonInput(
                  controller: _geoController,
                  hint: _int.getString(LocaleKey.locationKey),
                  prefixIcon: const CommonIcon(Icons.location_on),
                  onTap: _validateLocationPlatform,
                ),
              ],
            ),
          ),
          const SizedBox(height: 50),
          CommonButton(
            text: _int.getString(LocaleKey.updateKey),
            callback: _updateInfo,
          ),
        ],
      ),
    );
  }

  void _initPage() {
    _pr = ProgressDialog(context);

    user = _userProvider.getUser;

    _nameController!.text = user!.getName;
    _phoneController!.text = user!.getPhone;
    _addressController!.text = user!.getAddress;
    _descriptionController!.text = user!.getDescription;

    if (user!.getHomeLocation != null) {
      _geoController!.text =
          '${user!.getHomeLocation!.latitude} / ${user!.getHomeLocation!.longitude}';
    }

    setState(() {});
  }

  // Image Functions
  void _validateLocationImage() async {
    if (kIsWeb) {
      _requestWebLocation();
    } else {
      _getMobileImage();
    }
  }

  void _getMobileImage() async {
    try {
      final imageFile = await ImageUtils().getMobileImage(context);

      if (imageFile != null) _userImage = imageFile;
    } catch (e) {
      print(e);
    }
    setState(() {});
  }

  // Location Functions
  void _validateLocationPlatform() {
    _pr.show();
    if (kIsWeb) {
      _requestWebLocation();
    } else {
      _checkLocationPermissions();
    }
  }

  void _requestWebLocation() {
    /*webLocation.getCurrentPosition(allowInterop((GeolocationPosition pos) {
      final double latitude = pos.coords.latitude;
      final double longitude = pos.coords.longitude;

      _geoController.text = "$latitude / $longitude";

      geoPoint = GeoPoint(latitude, longitude);
      setState(() {});
      _pr.hide();
    })); */
  }

  void _checkLocationPermissions() async {
    LocationPermission permission = await Geolocator.checkPermission();

    if (permission == LocationPermission.always ||
        permission == LocationPermission.whileInUse) {
      _checkAppLicationEnable();
    } else {
      permission = await Geolocator.requestPermission();

      if (permission == LocationPermission.always ||
          permission == LocationPermission.whileInUse) {
        _checkAppLicationEnable();
      } else {
        _pr.hide();
        commonOkDialog(
          context,
          _int.getString(LocaleKey.needLocationAccessKey),
        );
      }
    }
  }

  void _checkAppLicationEnable() async {
    bool isLocationServiceEnabled = await Geolocator.isLocationServiceEnabled();

    if (isLocationServiceEnabled) {
      _requestLocation();
    } else {
      await Geolocator.openLocationSettings();

      isLocationServiceEnabled = await Geolocator.isLocationServiceEnabled();
      if (isLocationServiceEnabled) {
        _requestLocation();
      } else {
        _pr.hide();
        commonOkDialog(context, _int.getString(LocaleKey.needDevieLocationKey));
      }
    }
  }

  void _requestLocation() async {
    final Position position = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );

    _geoController!.text = '${position.latitude} / ${position.longitude}';

    geoPoint = GeoPoint(position.latitude, position.longitude);
    _pr.hide();
  }

  // Update User Info Functions
  void _updateInfo() async {
    if (_formKey.currentState!.validate()) {
      if (_userImage != null) {
        _updateImage();
      } else {
        _updateForm();
      }
    }
  }

  void _updateImage() async {
    _pr.show();

    final int size = _userImage!.lengthSync();

    final compressImage =
        await ImageUtils().testCompressAndGetFile(_userImage, size);

    if (compressImage == null) {
      _pr.hide();
      commonOkDialog(context, 'Error during commpression');
      return;
    }

    _userImage = compressImage;

    final String? imageUrl = await _storageProvider.updateImageFile(
      'imageProfile/${user!.getId}',
      _userImage,
    );

    if (imageUrl != null) {
      _pr.hide();
      _userProvider.getUser!.setProfilePicture = imageUrl;

      _updateForm();
    } else {
      _pr.hide();
      commonOkDialog(context, _int.getString(LocaleKey.processFailedKey));
    }
  }

  void _updateForm() async {
    _pr.show();
    user!.setName = _nameController!.text;
    user!.setPhone = _phoneController!.text;
    user!.setAddress = _addressController!.text;
    user!.setDescription = _descriptionController!.text;

    if (geoPoint != null) user!.setHomeLocation = geoPoint;

    await _userProvider.updateUser(user!).then((value) {
      _userProvider.setUser = user;

      _pr.hide();
      commonOkDialog(
        context,
        _int.getString(LocaleKey.successUpdateKey),
        okFunction: () => RouteUtils.popPage(),
      );
    }).catchError((error) {
      _pr.hide();
      commonOkDialog(context, _int.getString(LocaleKey.processFailedKey));
    });
  }
}
