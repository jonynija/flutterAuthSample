import 'package:flutter/material.dart';

import 'package:auth/model/user/user.dart';
import 'package:auth/services/user/user_service.dart';

class UserProvider with ChangeNotifier {
  final UserService _service = UserService();

  User? _user;

  /* Getters & Setters */

  User? get getUser => _user;
  set setUser(User? user) => _user = user;

  /* Services */
  Future<User?> requestUser(String id) async {
    final userDoc = await _service.getUser(id);

    if (!userDoc.exists) return null;
    return User.fromMap(userDoc.data() as Map<String, dynamic>, userDoc.id);
  }

  Future<void> createUser(User user) async {
    return _service.createUser(user);
  }

  Future<void> updateUser(User user) async {
    return _service.updateUser(user);
  }

  Future<void> updateImageProfile(String userUid, String imageUrl) async {
    return _service.updateImageProfile(userUid, imageUrl);
  }
}
