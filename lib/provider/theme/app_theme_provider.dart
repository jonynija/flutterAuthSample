import 'package:flutter/material.dart';

import 'package:auth/utils/theme_codes.dart';

class AppThemeProvider with ChangeNotifier {
  ThemeMode? _mode;

  ThemeMode? get getCurrentMode => _mode;

  set setCurrentMode(ThemeMode? mode) {
    _mode = mode;
    notifyListeners();
  }

  String getThemeStr(ThemeMode? mode) {
    switch (mode) {
      case ThemeMode.dark:
        return ThemeCodes.darkTheme;
      case ThemeMode.light:
        return ThemeCodes.ligthTheme;
      default:
        return ThemeCodes.systemTheme;
    }
  }

  ThemeMode getThemeMode(String? mode) {
    switch (mode) {
      case ThemeCodes.darkTheme:
        return ThemeMode.dark;
      case ThemeCodes.ligthTheme:
        return ThemeMode.light;
      default:
        return ThemeMode.system;
    }
  }
}
