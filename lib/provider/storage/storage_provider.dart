import 'dart:io';

import 'package:flutter/material.dart';

import 'package:auth/services/storage/storage_service.dart';

class StorageProvider with ChangeNotifier {
  final StorageService _service = StorageService();

  Future<String?> updateImageFile(String path, File? file) async {
    return await _service.updateImageFile(path, file);
  }
}
