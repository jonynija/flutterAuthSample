import 'package:flutter/material.dart';

import 'package:auth/common/dialogs/common_dialogs.dart';

class OnCloseApp extends StatefulWidget {
  final Widget child;

  const OnCloseApp({
    super.key,
    required this.child,
  });

  @override
  State<StatefulWidget> createState() => _OnCloseAppState();
}

class _OnCloseAppState extends State<OnCloseApp> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: widget.child,
    );
  }

  Future<bool> _onWillPop() async {
    return (await showExitApp(context)) ?? false;
  }
}
