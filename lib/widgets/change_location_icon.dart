import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:auth/app.dart';
import 'package:auth/localization/internationalization.dart';
import 'package:auth/utils/Utils.dart';

class ChangeLocaleIcon extends StatefulWidget {
  const ChangeLocaleIcon({
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _ChangeLocaleIconState();
}

class _ChangeLocaleIconState extends State<ChangeLocaleIcon> {
  late SharedPreferences _prefs;
  late Internationalization _int;

  List<String> languages = [
    LocaleCodes.englishLocale,
    LocaleCodes.spanishLocale,
  ];
  String? currentLanguage;

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, initWidet);
  }

  @override
  Widget build(BuildContext context) {
    _int = Internationalization(context);

    return PopupMenuButton(
      itemBuilder: (_) => <PopupMenuEntry<String>>[
        PopupMenuItem(
          value: null,
          child: Text(
            _int.getString(LocaleKey.settingLanguageKey)!,
          ),
        ),
        const PopupMenuDivider(
          height: 10,
        ),
        CheckedPopupMenuItem(
          value: LocaleCodes.englishLocale,
          checked: currentLanguage == LocaleCodes.englishLocale,
          child: Text(
            _int.getString(LocaleKey.englishKey)!,
          ),
        ),
        CheckedPopupMenuItem(
          value: LocaleCodes.spanishLocale,
          checked: currentLanguage == LocaleCodes.spanishLocale,
          child: Text(
            _int.getString(LocaleKey.spanishKey)!,
          ),
        ),
      ],
      icon: const Icon(
        Icons.language,
        color: Colors.white,
      ),
      onSelected: _updateLanguage,
    );
  }

  void initWidet() async {
    _prefs = await SharedPreferences.getInstance();

    currentLanguage = _prefs.getString(LocaleCodes.locationSaved);
    setState(() {});
  }

  void _updateLanguage(String locale) {
    currentLanguage = locale;

    Locale newLocale = Locale(locale);

    _saveLocale();

    MyApp.setLocale(context, newLocale);
    setState(() {});
  }

  void _saveLocale() async {
    _prefs.setString(LocaleCodes.locationSaved, currentLanguage!);
  }
}
