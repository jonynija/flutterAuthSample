import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';

import 'package:auth/widgets/circular_widget.dart';

class LoadingImage extends StatefulWidget {
  final String url;
  final BoxFit fit;

  final double? height;
  final double? width;

  final bool circle;

  const LoadingImage(
    this.url, {
    super.key,
    this.fit = BoxFit.fill,
    this.height,
    this.width,
    this.circle = false,
  });

  @override
  State<StatefulWidget> createState() => _LoadingImageState();
}

class _LoadingImageState extends State<LoadingImage> {
  @override
  Widget build(BuildContext context) {
    final Widget image = CachedNetworkImage(
      imageUrl: widget.url,
      width: widget.width,
      height: widget.height,
      placeholder: (context, url) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      },
      errorWidget: (context, url, error) {
        double size = widget.height! / 2;

        if (size < 50) size = widget.height!;

        return Center(
          child: Icon(
            Icons.error,
            size: size,
          ),
        );
      },
      fit: widget.fit,
    );

    return (!widget.circle) ? image : CircularWidget(child: image);
  }
}
