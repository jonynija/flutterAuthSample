import 'package:flutter/material.dart';

import 'package:auth/localization/internationalization.dart';
import 'package:auth/utils/Utils.dart';
import 'package:auth/widgets/change_location_icon.dart';

class CommonAppbar extends StatefulWidget implements PreferredSizeWidget {
  final bool showLanguage;
  final Widget? trailing;

  const CommonAppbar({
    super.key,
    this.showLanguage = false,
    this.trailing,
  });

  @override
  State<StatefulWidget> createState() => _CommonAppbarState();

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class _CommonAppbarState extends State<CommonAppbar> {
  late Internationalization _int;

  @override
  Widget build(BuildContext context) {
    _int = Internationalization(context);

    return AppBar(
      title: Text(
        _int.getString(LocaleKey.samplesKey)!,
        style: Theme.of(context).textTheme.displayLarge,
      ),
      centerTitle: true,
      actions: [
        (widget.showLanguage) ? const ChangeLocaleIcon() : const SizedBox(),
        (widget.trailing != null) ? widget.trailing! : const SizedBox()
      ],
    );
  }
}
