import 'package:flutter/material.dart';

class CommonButton extends StatefulWidget {
  final Function callback;
  final String? text;

  const CommonButton({
    super.key,
    required this.text,
    required this.callback,
  });

  @override
  CommonButtonState createState() => CommonButtonState();
}

class CommonButtonState extends State<CommonButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 2, 0, 2),
      width: double.infinity,
      child: TextButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(
            Theme.of(context).colorScheme.secondary,
          ),
        ),
        child: Text(
          widget.text!,
          style: Theme.of(context).textTheme.labelLarge,
          textAlign: TextAlign.center,
        ),
        onPressed: () => widget.callback(),
      ),
    );
  }
}
