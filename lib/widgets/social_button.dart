import 'package:flutter/material.dart';

import 'package:auth/common/dialogs/common_dialogs.dart';
import 'package:auth/enums/auth_type.dart';
import 'package:auth/localization/internationalization.dart';
import 'package:auth/utils/Utils.dart';

class SocialButton extends StatefulWidget {
  final Function? callback;
  final AuthType type;

  const SocialButton({
    super.key,
    required this.callback,
    required this.type,
  });

  @override
  State<StatefulWidget> createState() => _SocialButtonState();
}

class _SocialButtonState extends State<SocialButton> {
  late Internationalization _int;

  TextStyle? textStyle;
  Color? background;

  String? image;
  late String text;

  @override
  void initState() {
    super.initState();

    switch (widget.type) {
      case AuthType.apple:
        textStyle = const TextStyle(color: whiteColor);
        background = blackColor;
        text = 'Apple';
        break;
      case AuthType.google:
        textStyle = const TextStyle(color: Colors.black);
        background = googleColor;
        text = 'Google';
        break;
      case AuthType.twitter:
        textStyle = const TextStyle(color: Colors.black);
        background = twitterColor;
        text = 'Twitter';
        break;
      case AuthType.facebook:
        textStyle = const TextStyle(color: Colors.white);
        background = facebookColor;
        text = 'Facebook';
        break;
      case AuthType.github:
        textStyle = const TextStyle(color: Colors.white);
        background = githubColor;
        text = 'Github';
        break;
      default:
        break;
    }
    image = text.trim().toLowerCase();
  }

  @override
  Widget build(BuildContext context) {
    _int = Internationalization(context);

    return Container(
      width: double.infinity,
      margin: const EdgeInsets.symmetric(vertical: 2),
      child: TextButton(
        onPressed: widget.callback as void Function()? ?? _showAlert,
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color?>(background),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              'assets/social/$image.png',
              width: 30,
              height: 30,
            ),
            const SizedBox(width: 10),
            Text(
              '$text ${_int.getString(LocaleKey.loginKey)}',
              style: textStyle,
            ),
          ],
        ),
      ),
    );
  }

  void _showAlert() {
    commonOkDialog(
      context,
      Internationalization(context).getString(LocaleKey.optionNotAvailableKey),
    );
  }
}
