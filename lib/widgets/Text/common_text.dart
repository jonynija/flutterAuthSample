import 'package:flutter/material.dart';

class CommonText extends StatefulWidget {
  final String? text;
  final TextStyle? style;

  const CommonText(
    this.text, {
    super.key,
    this.style,
  });

  @override
  State<StatefulWidget> createState() => _CommonTextState();
}

class _CommonTextState extends State<CommonText> {
  @override
  Widget build(BuildContext context) {
    return Text(
      widget.text ?? '',
      style: widget.style ?? Theme.of(context).textTheme.bodyLarge,
      textAlign: TextAlign.center,
    );
  }
}
