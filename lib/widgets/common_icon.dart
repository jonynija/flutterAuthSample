import 'package:flutter/material.dart';

import 'package:auth/utils/colors.dart';

class CommonIcon extends StatefulWidget {
  final IconData icon;
  final bool isWhite;
  final Color? customColor;
  final double? size;

  const CommonIcon(
    this.icon, {
    super.key,
    this.customColor,
    this.isWhite = false,
    this.size,
  });

  @override
  State<StatefulWidget> createState() => _CommonIconState();
}

class _CommonIconState extends State<CommonIcon> {
  @override
  Widget build(BuildContext context) {
    return Icon(
      widget.icon,
      color: (widget.customColor != null)
          ? widget.customColor
          : (widget.isWhite)
              ? whiteColor
              : Theme.of(context).primaryColor,
      size: widget.size,
    );
  }
}
