import 'package:flutter/material.dart';

class Route {
  final String _route;
  final StatefulWidget _page;
  const Route(this._route, this._page);

  String get getRoute => _route;
  StatefulWidget get getPage => _page;

  // set setRoute(String route) => _route = route;
  // set setPage(StatefulWidget page) => _page = page;
}
