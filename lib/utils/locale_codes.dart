class LocaleCodes {
  const LocaleCodes._();

  static const String englishLocale = 'en';
  static const String spanishLocale = 'es';

  static const String locationSaved = 'locationSaved'; // sharedPreferences
}

class LocaleKey {
  const LocaleKey._();

  static const String keyNotFound = 'keyNotFound';
  static const String emailKey = 'email';
  static const String passwordKey = 'password';
  static const String continueKey = 'continue';
  static const String loginKey = 'login';
  static const String signinAnonymusKey = 'signinAnonymus';
  static const String registerKey = 'register';
  static const String personalInfoKey = 'personalInfo';
  static const String emptyFieldKey = 'emptyField';
  static const String validateEmailKey = 'validateEmail';
  static const String logoutKey = 'logout';
  static const String samplesKey = 'samples';
  static const String userNotFoundKey = 'userNotFound';
  static const String registeredDifferentMethodKey =
      'registeredDifferentMethod';
  static const String wrongPasswordKey = 'wrongPassword';
  static const String cancelKey = 'cancel';
  static const String acceptKey = 'accept';
  static const String areYouSureKey = 'areYouSure';
  static const String exitAppKey = 'exitApp';
  static const String optionNotAvailableKey = 'optionNotAvailable';
  static const String settingLanguageKey = 'settingLanguage';
  static const String englishKey = 'english';
  static const String spanishKey = 'spanish';
  static const String darkThemeKey = 'darkTheme';
  static const String ligthThemeKey = 'ligthTheme';
  static const String systemThemeKey = 'systemTheme';
  static const String updateDataKey = 'updateData';
  static const String nameKey = 'name';
  static const String phoneKey = 'phone';
  static const String addressKey = 'address';
  static const String descriptionKey = 'description';
  static const String updateKey = 'update';
  static const String successUpdateKey = 'successUpdate';
  static const String processFailedKey = 'processFailed';
  static const String locationKey = 'location';
  static const String noLocationSavedKey = 'noLocationSaved';
  static const String needLocationAccessKey = 'needLocationAccess';
  static const String needDevieLocationKey = 'needDevieLocation';
  static const String homeKey = 'home';
  static const String appFailedKey = 'appFailed';
  static const String emailPasswordSigninKey = 'emailPasswordSignin';
  static const String signinAnonymusAlertKey = 'signinAnonymusAlert';
}
