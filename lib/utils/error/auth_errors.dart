import 'package:flutter/material.dart';

import 'package:auth/common/dialogs/common_dialogs.dart';
import 'package:auth/localization/internationalization.dart';
import 'package:auth/utils/locale_codes.dart';

class AuthErrors {
  void showUserError(BuildContext context, String err) {
    final intl = Internationalization(context);

    String? error = intl.getString(LocaleKey.processFailedKey);
    bool showError = true;

    switch (err) {
      case 'user-not-found':
        error = intl.getString(LocaleKey.userNotFoundKey);
        break;
      case 'account-exists-with-different-credential':
        error = intl.getString(LocaleKey.registeredDifferentMethodKey);
        break;
      case 'wrong-password':
        error = intl.getString(LocaleKey.wrongPasswordKey);
        break;
      case 'popup-closed-by-user':
        showError = false;
        break;
      default:
        print(err);
    }

    if (showError) commonOkDialog(context, error);
  }
}
