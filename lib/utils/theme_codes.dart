class ThemeCodes {
  const ThemeCodes._();

  static const String ligthTheme = 'LigthTheme';
  static const String darkTheme = 'DarkTheme';
  static const String systemTheme = 'SystemTheme';

  static const String themeSaved = 'ThemeSaved'; // sharedPreferences
}
