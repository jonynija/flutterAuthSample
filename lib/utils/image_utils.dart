import 'dart:io';

import 'package:flutter/material.dart';

import 'package:image_picker/image_picker.dart';

import 'package:auth/widgets/common_icon.dart';

class ImageUtils {
  Future<File?> getMobileImage(BuildContext context) async {
    final choice = await _displayBottomSheet(context);

    if (choice != null) {
      final pickedFile = await ImagePicker().pickImage(
        source: (choice == 1) ? ImageSource.camera : ImageSource.gallery,
        maxWidth: 1024,
        maxHeight: 1024,
      );

      if (pickedFile != null) {
        return File(pickedFile.path);
      }
    }
    return null;
  }

  Future<int?> _displayBottomSheet(BuildContext context) async {
    int? result;
    await showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          left: true,
          bottom: true,
          right: true,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                // leading: Icon(Icons.camera),
                leading: const CommonIcon(
                  Icons.camera,
                  isWhite: true,
                ),
                title: const Text('Camera'),
                onTap: () => Navigator.pop(context, 1),
              ),
              ListTile(
                // leading: Icon(Icons.collections),
                leading: const CommonIcon(
                  Icons.collections,
                  isWhite: true,
                ),
                title: const Text('Gallery'),
                onTap: () => Navigator.pop(context, 2),
              ),
            ],
          ),
        );
      },
    ).then((value) {
      result = value;
    });
    return result;
  }

  Future<File?> testCompressAndGetFile(File? file, size) async {
    try {
      // TODO:  temp to reimplement compress method
      /// If file is grater than 1gb
      /* if (size >= 100000) {
        final quality = (10000000 / size).round();

        final imageCompressed = await FlutterImageCompress.compressAndGetFile(
          file?.path ?? '',
          targetPath,
          quality: quality,
        );

        return File(imageCompressed!.path);
      } */

      return file;
    } catch (e) {
      print(e);
    }

    return null;
  }
}
