import 'package:flutter/material.dart';

import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:auth/localization/config_localization.dart';
import 'package:auth/provider/theme/app_theme_provider.dart';
import 'package:auth/routes/route_names.dart';
import 'package:auth/routes/routes.dart';
import 'package:auth/screens/utils/error/error_page.dart';
import 'package:auth/theme.dart';
import 'package:auth/utils/Utils.dart';
import 'package:auth/utils/theme_codes.dart';

class MyApp extends StatefulWidget {
  const MyApp({
    super.key,
  });

  static void setLocale(BuildContext context, Locale locale) {
    _MyAppState state = context.findAncestorStateOfType<_MyAppState>()!;
    state._setLocate(locale);
  }

  @override
  State<StatefulWidget> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late SharedPreferences _prefs;

  Locale _locale = const Locale(LocaleCodes.englishLocale);
  late AppThemeProvider _themeProvider;

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, _initConfigs);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<AppThemeProvider>(
      builder: (context, appProvider, child) {
        _themeProvider = appProvider;

        return MaterialApp(
          title: 'Auth Sample',
          debugShowCheckedModeBanner: false,

          /// Locale Config
          locale: _locale,
          supportedLocales: const [
            Locale(LocaleCodes.englishLocale),
            Locale(LocaleCodes.spanishLocale),
          ],
          localizationsDelegates: const [
            ConfigLocalization.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          localeResolutionCallback: (deviceLocale, supportedLocales) {
            for (var locale in supportedLocales) {
              if (locale.languageCode == deviceLocale!.languageCode &&
                  locale.countryCode == deviceLocale.countryCode) {
                return deviceLocale;
              }
            }
            return supportedLocales.first;
          },

          // / Theme Config
          theme: CustomTheme.getLigth,
          darkTheme: CustomTheme.getDark,
          themeMode: _themeProvider.getCurrentMode ?? ThemeMode.light,

          /// Routes Config
          navigatorKey: Routes.sailor.navigatorKey,
          onGenerateRoute: Routes.sailor.generator(),
          initialRoute: RouteNames.initialLoadingRoute,

          builder: (BuildContext context, Widget? widget) {
            ErrorWidget.builder = (FlutterErrorDetails errorDetails) {
              return ErrorPage(errorDetails);
            };

            return widget!;
          },
        );
      },
    );
  }

  void _setLocate(Locale locale) {
    _locale = locale;
    setState(() {});
  }

  void _initConfigs() async {
    _prefs = await SharedPreferences.getInstance();

    _configLanguage();
    _configTheme();
  }

  void _configLanguage() {
    final String? localeSaved = _prefs.getString(LocaleCodes.locationSaved);

    if (localeSaved != null && localeSaved.trim().isNotEmpty) {
      _setLocate(Locale(localeSaved));
    } else {
      Future.delayed(
        const Duration(seconds: 1),
        () => _prefs.setString(LocaleCodes.locationSaved, _locale.languageCode),
      );
    }
  }

  void _configTheme() {
    final String? themeSaved = _prefs.getString(ThemeCodes.themeSaved);

    _themeProvider.setCurrentMode = _themeProvider.getThemeMode(themeSaved);
  }
}
