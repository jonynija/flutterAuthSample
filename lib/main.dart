import 'package:flutter/material.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';

import 'package:auth/app.dart';
import 'package:auth/firebase_options.dart';
import 'package:auth/provider/storage/storage_provider.dart';
import 'package:auth/provider/theme/app_theme_provider.dart';
import 'package:auth/provider/user/user_provider.dart';
import 'package:auth/routes/routes.dart';

void main() async {
  // Create routes
  Routes.createRoutes();

  // Initialize firebase configs
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(
    ChangeNotifierProvider<AppThemeProvider>(
      create: (context) => AppThemeProvider(),
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => UserProvider()),
          ChangeNotifierProvider(create: (_) => StorageProvider()),
        ],
        child: const MyApp(),
      ),
    ),
  );
}
