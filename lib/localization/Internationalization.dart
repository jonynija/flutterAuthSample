import 'package:flutter/material.dart';

import 'package:auth/localization/config_localization.dart';

class Internationalization {
  ConfigLocalization? _localization;

  Internationalization(BuildContext context) {
    _localization = ConfigLocalization.of(context);
  }

  String? getString(key) {
    return _localization!.getTranslatedValue(key);
  }
}
